use <shapes.scad>

module outer_conducting_ring(height, diameter, width)
{
	color("yellow")
	{
		ring(height = height, outer_diameter = diameter, inner_diameter = diameter - width*2);
		translate([diameter*(0.5)-0.5,-0.25,height]) 
			rotate([0,90,0]) 
				cube([15,2,0.5]);
	}
}

outer_conducting_ring(height = 3, diameter = 50, width = 8);

