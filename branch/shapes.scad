use <threads.scad>

module hollow_thread(height, diameter, width)
{
	difference()
	{
		metric_thread(diameter=diameter, pitch=4, thread_size=3,length=height);
		translate([0,0,-1])
			cylinder(h=height+2,r = diameter/2 - width);
	}
}

module torus(diameter, width) 
{
	rotate_extrude() 
	{
		translate([diameter/2,0,0]) circle(r=width/2);
	}
}

module hollow_sphere(diameter, width) 
{
	difference()
	{
		sphere(diameter/2); 
		sphere(diameter/2 - width);
	}
}

module dome(diameter, width)
{
	difference() 
	{
		hollow_sphere(diameter, width);
		translate([0,0,-diameter/4]) 
			cube([diameter,diameter,diameter/2],center=true);
	}
}


module ring(height,outer_diameter, inner_diameter)
{
	difference()
	{
		cylinder(h=height,r=outer_diameter/2);
		translate([0,0,-height*0.1])
			cylinder(h=height*1.2,r=inner_diameter/2);
	}
}


module tube(height, diameter, width)
{
	difference() 
	{
		cylinder(h=height,r=diameter/2);
		translate([0,0,-1]) 
			cylinder(h=height+2,r=diameter/2-width);
	}
}

