use <threads.scad>
use <../base/base.scad>
use <body.scad>
use <copula.scad>

$fn = 128;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
height = 180;
base_height=diameter/10;
base_thread_height=10;
stand_height = 35;

color("silver")
    translate([0,0,height+80])
        copula(diameter=diameter,branch_angle=branch_angle,branch_diameter=branch_diameter,num_branches=num_branches);

color("white")
    translate([0,0,base_height+60])
        body(height-base_height,diameter,width=3);

for (i = [0:2])
{
    translate([0,0,260]) 
        rotate([0,-45,30+120*i]) 
            translate([0,0,50]) 
                color("white")
                   body(height = 111, diameter = 18.5, width = 3);
}    
 
color("white")
    base(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);


*body(height = 111, diameter = 18.5, width = 3);




     
 
