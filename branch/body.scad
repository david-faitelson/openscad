use <threads.scad>
use <../common/shapes.scad>
use <../common/fillet.scad>
use <../common/slice.scad>

$fn = 64;

module body(height, diameter, width)
{
	thread_length = 10;

    translate([0,0,0.1])
        bottom_thread(thread_length, 0.9*diameter, width+1);
    
    difference() 
    {
        tube(height, diameter, width);

        translate([0,0,height - thread_length+0.1])
            metric_thread(length=thread_length, diameter = diameter* 0.9,pitch = 2);
    }

    tube(4, diameter, 0.1*diameter/2+width+1);
    
    translate([0,0,4])
        inner_circular_fillet(radius=diameter/2-width,fillet_radius=0.1*diameter/2);
}

module bottom_thread(depth, diameter, width)
{
	translate([0,0,-depth])
		hollow_thread(height=depth, diameter = diameter, width = width, pitch = 2);
}

slice([0,1,0])
{
    union()
    {
        body(height = 180, diameter = 40, width = 3);
        translate([0,0,-190])
        body(height = 180, diameter = 40, width = 3);
    }
}

