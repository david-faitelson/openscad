use <shapes.scad>

module inner_conducting_ring(height, tab_height, diameter, width)
{
	color("yellow")
	{
		ring(height = height, outer_diameter = diameter, inner_diameter = diameter - width*2);
		for(i = [0:2]) 
		{
			rotate([0,0,i*360/3])
				translate([diameter*0.5,-sqrt(width)/2,0]) 
					rotate([0,-90,0]) 
						cube([tab_height,sqrt(width),0.5]);
		}
	}
}

inner_conducting_ring(height = 5, tab_height= 13, diameter = 50, width = 8);