use <threads.scad>
use <../common/shapes.scad>
use <outer_ring.scad>

module copula(diameter, branch_angle, num_branches, branch_diameter) 
{
	thread_length = 10;

	petiol_height=diameter/5;

	color("silver",0.99)
	{
		translate([0,0,-thread_length])
			hollow_thread(diameter=diameter*0.9, height=thread_length,width=8,pitch=2);

		difference()
		{
			dome(diameter, 0.075*diameter);
			for(i = [0:num_branches-1]) 
			{
				rotate([branch_angle,0,(i-0.5)*360/num_branches])
					translate([0,0,diameter*0.3]) 
					{
						cylinder(h=diameter,r=branch_diameter/2);
					}
			}
	}

	}
	for(i = [0:num_branches-1]) 
	{
		rotate([branch_angle,0,(i-0.5)*360/num_branches])
			translate([0,0,diameter/2 -petiol_height]) 
				petiol(height=petiol_height,diameter=branch_diameter+0.1);
	}

}

module petiol(height, diameter)
{
	ring_width = 20;

	color("silver")
	difference()
	{
		cylinder(h=height,r=diameter/2);
		translate([0,0,-0.1*height/*0.1*height+0.1*/]) 
		{
			metric_thread(diameter=diameter*0.9, pitch=2, length=1.2*height);
		}

	}
/*
	translate([0,0,(0.1*height+0.1 + 0.1)]) 
	{
		color("red")
			outer_conducting_ring(height=0.5,diameter=diameter*(2/3),width = diameter/ring_width);
		color("black")
			outer_conducting_ring(height=0.5,diameter=diameter*(1/3),width = diameter/ring_width);
	}
*/
}

$fn = 64;

copula(diameter = 40, branch_angle = 45, num_branches = 3, branch_diameter = (40 / sqrt(3)) * 0.8);


