use <threads.scad>

module stand(bottom_diameter, top_diameter, height, thread_height) 
{
	difference()
	{
		minkowski() 
		{
			linear_extrude(height=height, scale=top_diameter/bottom_diameter)
				circle(r=bottom_diameter/2);
			sphere(r=5);
		}
		translate([0,0,height-thread_height+5])
			metric_thread(diameter=top_diameter*0.95, pitch=4, thread_size=3,length=thread_height);	
		translate([0,0,-5 -3]) 
		{
			cylinder(h=height-thread_height+10,r=top_diameter/2);	
		}
		translate([top_diameter/2 - 5,-5,-5]) 
		{
			cube([(bottom_diameter-top_diameter)/2 + 10,10,10]);
		}
	}
}

stand(bottom_diameter = 100, top_diameter = 50, height = 40, thread_height = 10);