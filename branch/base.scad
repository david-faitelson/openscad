use <threads.scad>
use <shapes.scad>

module base(height, thread_height, diameter, width) 
{
	translate([0,0, -thread_height])
		difference() 
		{
			metric_thread(diameter=diameter*0.95, pitch=4, thread_size=3,length=thread_height);
			translate([0,0,5])
				cylinder(h=thread_height*2,r=diameter*0.95/2-2.5);
		}

	translate([0,0,0])
	{
		difference() 
		{
			tube(height=height,diameter=diameter, width=width);
			translate([0,0,height - thread_height + 0.1])
			metric_thread(diameter=diameter*0.95, pitch=4, thread_size=3,length=thread_height);

		}
	}
}

base(height=50,thread_height=30,diameter=130,width=8);