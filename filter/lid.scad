use <../vitamins/solenoid.scad>
use <../common/shapes.scad>
use <../common/slice.scad>
use <../common/fillet.scad>

$fn = 32;

//slice([0,0,-1])
    //slice([0,-1,0])
lid();

*
box_fillet(width=8,height=8,depth=4,fillet_radius=1)
   cube([8, 8, 8], center= true);
   
   
*!slice([0,1.1,0])solenoid_house();


module lid()
{
	rotate([0,90,0])
		rotate([0,0,30])
        {
			ring(height=4, outer_diameter=80,  outer_diameter_bottom=80, inner_diameter=75, nfaces=6);
			translate([0,0,3.5]) 
                ring(height=2, outer_diameter=80,  outer_diameter_bottom=80, inner_diameter=72, nfaces=6);
        }

    difference()
    {
        spokes();
        cube([8,12.1,12.5],center=true);
    }

    translate([-8.5,0,0])
        rotate([0,90,0]) solenoid(0);

    translate([-13.5+2,0,0])
        solenoid_house();
    
    translate([-34.0+0.05,0,0]) 
        piston();
}

module solenoid_house()
{
    difference()
    {
        cube([27,12+3,11.0+4],center=true);
        
        translate([1.5,0,0])
            cube([27,12+0.1,11.0+1.5],center=true);
        
        // screw holes
        
        translate([-2,8,-3])
            rotate([90,0,0])
                cylinder(h=4,d=1.5);
        translate([8,8,3])
            rotate([90,0,0])
                cylinder(h=4,d=1.5);
        
        // cable hole
        translate([-8,0,5])
            rotate([0,0,0])
                cylinder(h=3,d=5);
        
    }
    
    copy_mirror([0,1,0])
        translate([-12,-12.2/2,12/2])
            rotate([0,90,0])
                linear_fillet(length=12,radius=2);

}

module piston()
{
	difference()
	{
        rotate([0,-90,0])
        box_fillet(width=8,height=8,depth=9,fillet_radius=2)
        rotate([0,90,0])
		hull()
		{
			cylinder(h=8,d=8,center=true);
			translate([5,0,0])
            {
                cube([8, 8, 8], center= true);
            }
		}
		cylinder(h=9,d=3,center=true);
		cube([8,9,3.2],center=true);
	}
}


module spokes()
{       
	for(i = [0:5])
	{
		rotate([60*i,0,0])
		{
			cube([4,85,4],center = true);
			translate([0,40,0])
				rotate([0,90,0])
					cylinder(h=4,r=5,center=true);

		}
	}
}


