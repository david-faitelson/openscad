use <../common/receptacle.scad>
use <../common/shapes.scad>
use <../common/pin.scad>
use <../common/slice.scad>
//use <../vitamins/stepper.scad>
use <../vitamins/bellow.scad>
use <../vitamins/solenoid-driver.scad>
use <../vitamins/stepper-driver.scad>
use <lid.scad>
use <steppers/stepper.scad>
use <case.scad>
use <crank.scad>

/*

Nema 17 42x42x20 cm, weight 140g

*/

$fn=128;

arm_length = 27;

handle_length = 15;

handle_angle = map(sin($t*360),-1,1,-180,180);

arm_angle = asin(((handle_length-3)/arm_length)*sin(handle_angle));

bellow_height = arm_length*cos(arm_angle) +  (handle_length-3) * cos(handle_angle);

bellow_angle = asin((bellow_height-11-7)/(6*25));

//slice([1,0,0],size=130)
    slice([0,1,0],size=130)

        case();

translate([31,-8,-9]) 
	rotate([0,-90,0]) 
    {
        translate([0,8,0]) NEMA17(20);
    }


translate([14.0,33,-6]) 
		rotate([90,180,90])
			stepper_driver();

translate([14.0,-34,-6.0]) 
    rotate([0,-90,180])
		solenoid_driver();


// arm and handle
    
translate([0,0,-9])
	rotate([0,-90,0])
        crank(handle_angle=handle_angle, handle_length=handle_length, arm_length=arm_length);


*color("yellow",0.3)
	translate([0,0,15])
		rotate([0,180,0])
			bellow(opening_angle=bellow_angle);



