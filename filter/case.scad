use <../common/receptacle.scad>
use <../common/shapes.scad>
use <../common/pin.scad>
use <../common/slice.scad>
use <../common/math.scad>

$fn=128;

angle = 8; //map(x=sin($t*360),b1=-1,e1=1,b2=0,e2=8);

translate([0,0,12.2]) rotate([0,0,30+angle]) bellow_support();

//slice([1.2,0,0],size=130)
    //slice([0.0,0,0],size=130)
        case();


// modules

module case()
{

//    translate([0,0,12.2]) rotate([0,0,30]) bellow_support();

	color("green",0.9)translate([0,0,10.1])
		bellow_base();
	
    shell();
    
	difference()
	{
		hseparator();

		translate([7,0,-9]) 
        {
            // motor axis hole
            
			rotate([0,90,0]) 
				cylinder(h=5, r = 11.4);
        
            // stepper bolt holes
            stepper_holes();
        }

		// wires hole

		translate([7,0,-32]) 
			rotate([0,90,0]) 
				cylinder(h=5, r = 4.0);
        

	}

	translate([5,0,-37])
	{
		rotate([0,-90,90])
		{
			pipe(angle=90, radius = 5, width = 10, wall_width=2);

		}
		translate([0,0,5])
            rotate([0,90,0]) 
                tube(height=6,diameter=10,width=1);
	}

    
	stepper_driver_pins();
    
    solenoid_driver_pins();

}

module shell()
{
	rotate([180,0,60])
	{
		difference()
		{
			hollow_receptacle();
			translate([0,0,37])
				cylinder(h=3,r=4,center=true);
		}
	
		translate([0,0,38.85])
            rotate([180,0,30])
                hollow_hexagonal_base(start_height=22,end_height=29-0.1,radius=29);			
	}
}

module bellow_support()
{
    color("orange")
        ring(height=2,outer_diameter=96,outer_diameter_bottom=96, inner_diameter=80, nfaces =6); 
    
    color("orange")
    for(i=[0:5])
    {
        rotate([0,0,30+i*360/6])
            translate([42,0,2]) 
            {
                scale([1,1,-1])
                {
                    cylinder(h=6,r=2-0.1);
                    translate([0,0,4]) cylinder(h=2,r=3-0.1);
                }
            }
    }
}

module bellow_base()
{
    rotate([0,0,30])
    {
        difference()
        {
            ring(height=2, outer_diameter=118, outer_diameter_bottom=115,inner_diameter=80, nfaces=6);

            for(i=[0:5])
            {
                rotate([0,0,30+i*360/6])
                    translate([42,0,-0.5]) 
                    {
                        cylinder(h=3,r=2);    
                    }
                rotate([0,0,30+8+i*360/6])
                    translate([42,0,-0.5]) 
                    {
                        cylinder(h=3,r=3);
                    }
            }
            
            translate([0,0,-0.5])
            for(i=[0:5])
            {
                rotate([0,0,30+i*360/6])
                {
                    rotate_extrude(angle=8)
                        translate([40,0]) square([4,3]);
                }
            }
        }
    }
}


module hseparator() 
{
	difference()
	{
		intersection()
		{
			translate([9.5,0,-10])
				cube([3,100,60],center=true);
			rotate([0,180,0]) receptacle(end_height=61.5);
		}
	
		translate([0,0,17])
            cube([120,120,9.9],center=true);
        
        translate([7,-2.5,1])
            cube([5,5,12]);
	}
}

module stepper_holes()
{
    for(i=[-1:2:1])
        for(j=[-1:2:1])
        {
            translate([0,15*i,15*j])
                rotate([0,90,0]) cylinder(h=5,d=2.8);
        }
}

module stepper_driver_pins()
{
	translate([15.0,31,-6.5]) 
		rotate([0,-90,0])
            {
                translate([-11.1, -6.35,0])
					pin(6,2.0-0.1);
                translate([-11.1, 11.45,0])
					pin(6,2.0-0.1);
                translate([11.1, 10.8,0])
					pin(6,2.0-0.1);
            }
}

module solenoid_driver_pins()
{
	translate([15.0,-34,-4.7]) 
		rotate([0,-90,0])
			for(i = [-1:2:1])
				for(j = [-1:2:1])
				{
					translate([i*(26.67/2 - 2),j*(19.05/2 - 2),0])
						pin(6,2.0-0.1);
				}
}
