use <../common/shapes.scad>
use <../common/pin.scad>
use <../common/slice.scad>
use <BOLTS/BOLTS.scad>
use <lid.scad>

$fn=128;

!shaft();

//slice([0,1,0])
    crank(handle_angle=0, handle_length=15, arm_length=27);

*translate([-7.8,0,-2]) rotate([0,90,0]) DIN933("M3",6);
*translate([-6.3,0,-2]) rotate([30,0,0])rotate([0,90,0]) DIN934("M3");

module crank(handle_angle, handle_length, arm_length)
{

    arm_angle = asin(((handle_length-3)/arm_length)*sin(handle_angle));
	
    rotate([0,0,handle_angle])
    {
        color("purple")
        {
            translate([0,0,-7])
                handle(length = handle_length);
        }

        translate([handle_length-2.5,0,4.75])
        {

            rotate([0,0,-handle_angle - arm_angle])
            {
                arm(length=arm_length,width= 6)
                    rotate([0,0,arm_angle])	
                        union()
                        {
                            translate([34,0,0])
                            {
                                *lid();
*								translate([3,0,0])
                                    rotate([0,90,0])
                                        rotate([0,0,30]) cloth();

                            }
                        }
            }
        }
    }
	
}


// modules

module cloth()
{
	color("black", 0.05)
		cylinder(h=2, d=80, $fn=6);
}


module shaft()
{
    translate([0,0,4.8])
        scale([1,1,-1])
            pin(length=5, width=3);
    translate([0,0,-4.8])
        pin(length=5, width=3);
}

module arm(length, width)
{
	color("SeaGreen",0.9)
        difference()
        {
            hull()
            {
                cylinder(h=3,d=width,center=true);
                translate([length,0,0])
                {
                    cylinder(h=3,d=width,center=true);
                }
            }
            translate([0,0,-2]) 
                cylinder(h=4,d=3.0);
            translate([length,0,-2.1]) 
                cylinder(h=4.2,d=3.1);
        }
        
        translate([length,0,0])
        {
            *shaft();
            children(0);
        }

}

module handle(length)
{
	difference()
	{
        union()
        {

            cylinder(h=5,d=20);
            translate([0,0,5-0.01])
                handle_face(length=length,width=5);
        }

        difference()
        {
            translate([0,0,-0.1])
                cylinder(h=5+5+1,r=2.52);

            translate([-2.5-0.1,-2.5,0])
                cube([0.51,5,10]);
        }
        
        translate([-5.1,0,5+1])
            cube([get_dim(DIN934_dims("M3"),"m_max"), get_dim(DIN934_dims("M3"),"s"), get_dim(DIN934_dims("M3"),"e_min")+2], center=true);

       translate([-10,0,5])
            rotate([0,90,0])
            {
                cylinder(h=get_dim(DIN933_dims("M3"),"k")+0.2,d=get_dim(DIN933_dims("M3"),"e"));
                cylinder(h=8,d=get_dim(DIN933_dims("M3"),"d1"));
            }
	}

    // arm axis
    
	translate([length-5/2,0,5+5-0.2])
	{
		cylinder(h=5,d=2.8);
        translate([0,0,5-1.25])
            cylinder(h=2,d=6);
	}
    
    
}

module handle_face(length, width) 
{
    hull()
    {
        translate([length-5,0,0]) 
            cylinder(h=width,r=5);
        cylinder(h=width,r=10);
    }    
}