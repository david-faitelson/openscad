use <../common/shapes.scad>

$fn = 32;


tube(height=10, diameter=10,width=1);

color("green")
    cylinder(h=20, d=2);



for(i=[0:360/7:360])
{
    rotate([0,0,i])
        translate([2.5,0,0])
            cylinder(h=20,d=2);
}

color("green")
    cylinder(h=20, d=2);    