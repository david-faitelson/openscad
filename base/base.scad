use <threads.scad>
use <../common/receptacle.scad>
use <../common/slice.scad>
use <../common/pin.scad>
use <../vitamins/power-socket.scad>
use <../common/shapes.scad>
use <../common/fillet.scad>

$fn = 64;

// slider

p = 0.618; // [0.05:0.05:0.5]

// slider

start_height = -30; // [ -90:1:0 ]

// slider

end_height = 60; // [ 0:1:180 ]

*translate([0,0,30])
{
	translate([0,0,10])
		cylinder(h=180,r=20);
}

slice([1,0,0],size=400)
union()
{
    translate([0,0,12])
        controller_board();
 
    base(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);

}


/*

R cos (alpha) = trunk_radius

alpha = acos (trunk_radius/R)

h = R sin(alpha)

*/

*base_column(height=20,diameter=20,column_width=3,fillet_radius=1);

module base(start_height = -30, end_height=60, step_size = 5, squash = 0.2, trunk_radius=20, trunk_thread_length=12)
{
    
    sphere_radius = (2/15)*end_height*sqrt(3);

    hole_height = sphere_radius * sin( acos(trunk_radius/sphere_radius));

    difference()
    {
        
        color("white",0.99)
            base_shell(start_height = start_height, end_height = end_height, step_size = step_size, squash= squash);

        // cut the top of the sphere
        
        translate([0,0, 10 + hole_height+sphere_radius])
            cube([sphere_radius*2,sphere_radius*2,sphere_radius*2],center=true);
        
        // create a thread in the sphere
        
        translate([0,0, 10 + hole_height+0.1])
            scale([1,1,-1])
                metric_thread(length=trunk_thread_length, diameter = trunk_radius*2,pitch = 2);

        // cut a hole for the power cable
        
        translate([0,-140, start_height*squash]) rotate([90,0,0]) cylinder(h=12,r=5);
    }
    
    echo(2*sphere_radius);
    
    // add a column to suppor the shell and hold the pcb
    
    translate([0,0,start_height*squash])
        base_column(height=squash*(end_height-start_height)-2,diameter=2*sphere_radius-3, column_width=8,fillet_radius=4);
    
}

module base_column(height, diameter, column_width, fillet_radius)
{
    difference()
    {
        tube(height=height,diameter=diameter,width = column_width);
        translate([0,0,-0.1]) cylinder(h=20,d=61.5);
        
        // hole for power socket
        
        translate([0,-diameter/2 - 4.1, height/4])
            rotate([-90,0,0])
            {
                intersection()
                {
                    translate([0,0,column_width/2-0.5])cylinder(h=column_width/2,d=8);
                    translate([-4,-6.5/2,column_width/2-0.5])cube([8,6.5,column_width/2]);
                }
            }
    }
    
    translate([0,0,height-1.5])
        scale([1,1,-1])
            inner_circular_fillet(angle=360, radius = diameter/2-column_width, fillet_radius = fillet_radius);
    
    for(i=[0:3])
    {
        rotate([0,0,360*i/4+45])
        {
            translate([27.31,0,height-20+3])
                pin(length=4,width=3.2);
        }
    }
    
    translate([0,-diameter/2 - 4.1, height/4])
        rotate([-90,0,0])power_socket();

}

module base_shell(start_height = -30, end_height=60, step_size = 5, squash = 0.2)
{
    sphere_radius = (2/15)*end_height*sqrt(3);
    
    difference()
    {
        union()
        {
            hexagonal_shape(squash,start_height,end_height,step_size);
            translate([0,0,10])
            {
                dome(diameter=sphere_radius*2);
            }
        }
        

        translate([0,0,-2])
            hexagonal_shape(squash,start_height,end_height-5,step_size);
    }
}

module hexagonal_shape(squash,start_height,end_height,step_size)
{
    rotate([0,0,30])
        scale([1,1,squash])
            hull()
                for(h = [start_height:step_size:end_height])
                {
                    translate([0,0,h])
                        linear_extrude(height=1)
                            hexagon(radius=sqrt(end_height*end_height-h*h));
                }
}

module controller_board()
{
    color("green")
        translate([-108.7,106.7,0]) import( "../electronics/controller/stl/controller-pcb.stl", convexity=3);
}
