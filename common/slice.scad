

module slice(direction=[1,0,0],size=100)
{
    difference()
    {
        children(0); 
        translate((size/2)*direction)cube([size,size,size],center=true);
    }
}

slice([0,-1.0,0])
    sphere(r=10);