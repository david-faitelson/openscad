use <threads.scad>

module hollow_thread(height, diameter, width, pitch=4)
{
	difference()
	{
		metric_thread(diameter=diameter, pitch=pitch,length=height);
		translate([0,0,-1])
			cylinder(h=height+2,r = diameter/2 - width);
	}
}

module torus(angle=360,diameter, width) 
{
	rotate_extrude(angle=angle) 
	{
		translate([diameter/2,0,0]) circle(r=width/2);
	}
}

module hollow_sphere(diameter, width) 
{
	difference()
	{
		sphere(diameter/2); 
		sphere(diameter/2 - width);
	}
}

module hollow_dome(diameter, width)
{
	difference() 
	{
		hollow_sphere(diameter, width);
		translate([0,0,-diameter/4]) 
			cube([diameter,diameter,diameter/2],center=true);
	}
}

module dome(diameter)
{
	difference() 
	{
		sphere(d=diameter);
		translate([0,0,-diameter/4]) 
			cube([diameter,diameter,diameter/2],center=true);
	}
}


module ring(height,outer_diameter, outer_diameter_bottom, inner_diameter, nfaces)
{
	difference()
	{
		cylinder(h=height,d1=outer_diameter, d2=outer_diameter_bottom, $fn=nfaces);
		translate([0,0,-height*0.1])
			cylinder(h=height*1.2,r=inner_diameter/2, $fn=nfaces);
	}
}


module tube(height, diameter, width)
{
	difference() 
	{
		cylinder(h=height,r=diameter/2);
		translate([0,0,-1]) 
			cylinder(h=height+2,r=diameter/2-width);
	}
}


module pipe(angle, radius, width, wall_width)
{
	difference()
	{
		torus(angle=angle, diameter=2*radius, width=width);
		rotate([0,0,-1]) torus(angle=angle+2, diameter=2*radius, width=width-wall_width);
	}
}

