module linear_fillet(length, radius)
{
    difference()
    {
        cube([length,radius,radius]);
        hull()
        {
            translate([0,radius,radius])
            sphere(r=radius);
            translate([length,radius,radius])
                sphere(r=radius);
        }
    }
}

module circular_fillet(angle=360,radius, fillet_radius)
{
    rotate_extrude(angle=angle)
    {
        translate([radius,0,0]) 
        {
            difference()
            {
                square(fillet_radius);
                translate([fillet_radius,fillet_radius]) 
                    circle(r=fillet_radius);
            }
        }
    }
}

module inner_circular_fillet(angle=360,radius, fillet_radius)
{
    rotate_extrude(angle=angle)
    {
        translate([radius-fillet_radius,0,0]) 
        {
            difference()
            {
                square(fillet_radius);
                translate([0,fillet_radius]) circle(r=fillet_radius);
            }
        }
    }
}


module box_fillet(width, height, depth, fillet_radius)
{
    copy_mirror([0,1,0])
    {
        copy_mirror([1,0,0])
        {
            union()
            {
            translate([width/2,height/2,-depth])
                circular_fillet(angle=90,radius=0.01,fillet_radius=fillet_radius);
            translate([width/2,0,-depth])
                rotate([0,0,-90])linear_fillet(length=height/2,radius=fillet_radius);
            translate([0,height/2,-depth])
                rotate([0,0,0])linear_fillet(length=width/2,radius=fillet_radius);
            }
        }
    }
    
    children(0);
}

module copy_mirror(normal)
{
    children(0);
    mirror(normal)
        children(0);
}


$fn=64;

cylinder(h=10,r=5);
circular_fillet(radius=5, fillet_radius=1);

translate([20,0,0])
{
    cylinder(h=10,r=5);
    circular_fillet(radius=5, fillet_radius=2);
    
}

translate([0,0,0])
{
    scale([1,0.5,1])
    circular_fillet(radius=5,fillet_radius=1);
}

translate([90,0,0])
{
    cylinder(r=5,h=10);
    {
        translate([5,5,1])
            box_fillet(width=10,height=10,depth=1,fillet_radius=1)
                cube([10,10,2],center=true);
        
    }
    translate([0,0,2])
        circular_fillet(angle=90,radius=5,fillet_radius=1);
	rotate([30,30,30]) {
        union()
        {
            cylinder(r=1.0,h=50,center=true);
            translate([0,0,9.8])
                rotate([0,180,-43]) 
                    funnel(length=2,start_radius=1,end_hradius=2,end_vradius=1.5,shift=0.0,angle=32.5, steps=30);
       }
   }

}

translate([60,0,0])
box_fillet(width=16,height=8,depth=4,fillet_radius=2)
    cube([16, 8, 8], center= true);
    
module funnel(length, start_radius, end_hradius, end_vradius, shift = 0, angle = 0,steps)
{
    b = end_hradius - start_radius - 1;
    
    for(i= [0:length/steps:length])
    {
        hull()
        {
            funnel_slice(i=i,shift=shift,angle=angle,end_hradius=end_hradius,end_vradius=end_vradius,start_radius=start_radius,resolution = 0.05,length=length);
            funnel_slice(i=i+1,shift=shift,angle=angle,end_hradius=end_hradius,end_vradius=end_vradius,start_radius=start_radius,resolution = 0.05,length=length);
        }
        
    }
}

module funnel_slice(i,shift,angle,end_hradius,end_vradius,start_radius,resolution,length)
{
    b = end_hradius - start_radius - 1;
    d = i/length;
    
    translate([shift*d,0,i])
        rotate([0,angle*d,0])
            linear_extrude(height=resolution)
                scale([1,1+((end_vradius/end_hradius)-1)*d])
                    circle(r=start_radius + b*d + d*d );
}