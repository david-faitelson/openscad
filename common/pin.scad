
module pin(length,width) 
{
	difference()
	{
		union()
		{
			scale([1,1/1.1,1])
				sphere(r=width*1.1/2);
			cylinder(h=length,r=width/2);
		}
		translate([0,0,-1])
			cube([0.4, width*1.1+1, 5],center=true);
	}
}
 
$fn = 32;

pin(5,4);