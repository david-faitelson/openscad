use <../common/slice.scad>
use <../branch/body.scad>

$fn = 64;

module hexagonal_base(start_height=16.5,end_height=25,radius=25)
{
    translate([0,0,-(start_height+end_height)/2])
    for(h = [start_height:1.0:end_height])
        {
            hull()
            {
                translate([0,0,h])
                    linear_extrude(height=0.01)
                        circle(r=radius - sqrt(radius*radius-h*h),$fn=6);
                translate([0,0,h+2.5])
                    linear_extrude(height=0.01)
                        circle(r=radius - sqrt(radius*radius-(h+2.5)*(h+2.5)),$fn=6);
            }
        }
}

module hollow_hexagonal_base(start_height=16.5,end_height=25,radius=25, hole_radius=4.0)
{
	difference()
	{
		hexagonal_base(start_height=start_height,end_height=end_height,radius=radius);
		scale(0.5) hexagonal_base(start_height=start_height,end_height=end_height,radius=radius);
		cylinder(h=1.1*(end_height-start_height),r=hole_radius,center=true);
	}
    
    translate([0,0,-(end_height-start_height)/2 +0.0])
        bottom_thread(depth=10, diameter=0.9*18.5, width=4.325);
}


module receptacle(start_height = -30, end_height=60, step_size = 5)
{
	rotate([0,0,30])
		scale([1,1,0.618])
			hull()
				for(h = [start_height:step_size:end_height])
				{
					translate([0,0,h])
						linear_extrude(height=1)
							hexagon(radius=sqrt(end_height*end_height-h*h));
				}

}

module hexagon(radius) 
{
	circle(r=radius,$fn=6);
}

module hollow_receptacle(start_height = -30, end_height=62, step_size = 5)
{
	difference()
	{
		receptacle(start_height,end_height,step_size);
		receptacle(start_height-2,end_height-2,step_size);
	translate([0,0,-start_height])
		cylinder(h=step_size+1,r=4);
	}
}

hollow_receptacle();

!slice([0,1,0])
    union()
    {
        color("white")hollow_hexagonal_base(start_height=22,end_height=29-0.1,radius=29);
        translate([0,0,-38])
            body(height = 22, diameter = 18.5, width = 2);
    };

