
module petiol()
{
	translate([0,0,-16.5])
	for(h = [16.5:1.0:25])
		{
			hull()
			{
				translate([0,0,h])
					linear_extrude(height=0.01)
						circle(r=25 - sqrt(25*25-h*h),$fn=6);
				translate([0,0,h+2.5])
					linear_extrude(height=0.01)
						circle(r=25 - sqrt(25*25-(h+2.5)*(h+2.5)),$fn=6);
			}
		}
}

module hollow_petiol()
{
	difference()
	{
		petiol();
		translate([0,0,-1])
			cylinder(h=10,r=4);
	}
}

hollow_petiol();