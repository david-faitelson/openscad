use <nose.scad>
use <../vitamins/vitamins.scad>
use <../common/receptacle.scad>

$fn = 50;

translate([0,0,8])
	dust_sensor();

translate([0,0,-1])
	rotate([0,0,30])
		nose_lid();

color("gray",0.99) 
	union()
	{
		translate([0,0,30])
			rotate([180,0,30])
				nose_base(height=27,radius=32);
		translate([0,0,30])
		difference() 
		{
			cylinder(h=4.6,r=8);
			translate([0,0,-0.1]) 
				cylinder(h=5.2,r=4.01);
		}
		hollow_receptacle();
	}


color("gray")
	translate([0,0,59.4])
		hollow_hexagonal_base();

/*
color("gray")
	translate([0,0,65])
	{
		rotate([0,0,30])
		translate([0,0,5])
			cylinder(d=12,h=150,$fn=12);
		rotate([0,0,30])
			cylinder(d=12,h=5,$fn=12);
	}

*/