
module nose_lid(height=3, radius=32)
{
	color("PaleVioletRed",0.99)
	difference()
	{
		cylinder(h=height,r=radius,$fn=6);
		translate([0,10,height-5])
			scale([1.618,1,1])
				cylinder(h=6,r=5,$fn=6);
		translate([0,-10,height-5])
			scale([1.618,1,1])
				cylinder(h=6,r=5,$fn=6);
	}

	color("black",0.4)
	translate([0,10,height-4])
		scale([1.618,1,1])
			cylinder(h=1,r=5,$fn=6);
	color("black",0.4)
	translate([0,-10,height-4])
		scale([1.618,1,1])
			cylinder(h=1,r=5,$fn=6);

}

module nose_base(height=30, radius=32)
{
	difference()
	{
		cylinder(h=height,r=radius,$fn=6);
		translate([0,0,1])
			cylinder(h=height+2,r=radius-2,$fn=6);
		translate([0,0,-1])
		cylinder(h=4, r=4);
	}
}

translate([0,0,30])
	nose_lid(height=3,radius=32);
nose_base(height=28, radius=32);

/*
difference()
{
	sphere(r=20);
	sphere(r=18);

	rotate([0,-60,0])
		translate([20,0,0])
			scale([2,2,1])
				sphere(r=3);

	rotate([0,-120,0])
		translate([20,0,0])
			scale([2,2,1])
				sphere(r=3);
}
*/

/*
$fn=64;

translate([0,0,35])
difference()
{
	sphere(r=20);
	sphere(r=18);
	for(angle = [0:-5:-180])
	{
		rotate([0,angle,0])
			translate([35,0,0])
				cube([45,50,0.5],center=true);
	}
}


cylinder(h=15,d1=13,d2=8);

*/