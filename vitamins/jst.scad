
module jst(pin_space=2,npins)
{
    translate([0,0,3])
    {
        color("white")
        difference()
        {
            cube([4.5, 2*(npins+0) + 1.9, 6],center=true);
            translate([0,0,1.1]) cube([3.0, 2*(npins+0), 5],center=true);
            translate([1,0,1.1]) cube([3.0, 2*(npins-1), 5],center=true);
            
        }
        
        color("silver")
        translate([0,-(npins-1)*pin_space/2,-1.5])
            for(i = [0:npins-1])
            {
                translate([0,i*pin_space,0])
                    cylinder(h=3.4,d=0.5);
            }
    }
}


jst(2,4);

translate([10,0,0])jst(2,6);
