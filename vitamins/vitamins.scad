$fn=30;

module tip120() {
    
    translate([0,0,4.1])
    {
    color("silver")
        difference()
        {
            translate([0,-10.53/2,15.75-6.5]) cube([1.4,10.53,6.5]);
            translate([-2.5,0,15.75-6.5/2]) rotate([0,90,0]) cylinder(h=5,d=4);
        }
        color("black")translate([0,-10.53/2,0])cube([4.83,10.53,15.75-6.5]);
    }
    color("silver")
    for(i=[0:2])
    {
        translate([1.52/2,-2.66+2.66*i,0]) cylinder(h=4.1,d=1.52);
    }
}

!tip120();

module controller() {
	// arduino nano
	color("blue")
		linear_extrude(5)
			square([17.78,43.2],center=true);
}

module driver() {
	// uln2003
	color("blue")
		linear_extrude(5)
			square([40.5,21.3],center=true);
}

module dust_sensor()
{
	color("silver",1)
	difference()
	{
		union()
		{
			hull()
			{
				translate([20,14,1]) 
					cylinder(h=2,r=3,center=true);
				translate([-20,14,1]) 
					cylinder(h=2,r=3,center=true);
				translate([20,-14,1]) 
					cylinder(h=2,r=3,center=true);
				translate([-20,-14,1]) 
					cylinder(h=2,r=3,center=true);
			}		
			
			translate([0,0,17.6/2])
				cube([46,30,17.6],center=true);

		}
		translate([3,-18.7,-0.5])
			cube([20,1+17-14.3,3]);
		translate([0,0,9])
			cylinder(h=20,d=8,center=true);

		translate([-24,17.5,12]) 
			rotate([45,0,0])
				cube([48,10,10]);

		translate([-24,-17.5,12]) 
			rotate([45,0,0])
				cube([48,10,10]);

		translate([-20,-15.3,17.6-3+0.1])
			cube([13.4, 9.3, 3]);



	}

	color("blue")
		translate([-20,-6.3-0.1,17.6-3+0.1])
			cube([13.4, 5, 3]);

//	translate([22.2,15-3,17.6-3]) cube([1,3,3]);
}

module red_wire(length=30) {
	color("red")
		wire(length);
}

module black_wire(length=30) {
	color("black")
		wire(length);
}

module wire(length=30) {
	cylinder(h=30,r=0.5);
}


module filter_cloth() {
	color("gray",0.5)
		linear_extrude(1) 
				circle(r=45,$fn=6);
}

*dust_sensor();