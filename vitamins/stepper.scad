$fn=50;

module stepper(angle) {
	// 28BYJ48 stepper motor
	color("gray") {
		difference() {
			hull() {
				translate([17.5,0,18]) cylinder(h=1,r=3.5);
				translate([-17.5,0,18]) cylinder(h=1,r=3.5);
			}
			translate([17.5,0,17]) cylinder(h=3,r=2);
			translate([-17.5,0,17]) cylinder(h=3,r=2);
		}

		cylinder(h=19,r=14);
	}
	color("dodgerBlue") translate([-14.6/2,-17,0.1]) cube([14.6,17,18.8]);

	translate([0,8,19-0.01]) 
	{
		color("silver") cylinder(h=1.5,r=4.5);
		color("gold") color("silver") cylinder(h=4,r=2.51);
		color("gold") 
			rotate([0,0,angle])
			difference() 
			{
				cylinder(h=10,r=2.5);
				translate([-5,1.5,-0.1]) cube([10,3,11]);
				translate([-5,-1.5-3,-0.1]) cube([10,3,11]);
			}
	}
}

module led(color_name = "white")
{
    color(color_name,0.5)
    union()
    {
       cylinder(h=4,r=2.5);
       translate([0,0,4])sphere(r=2.5);
    }
}

module stepper_driver()
{
    translate([-6,5,5])
    {
        // motor connector box
        
        color("white")
            difference()
            {
                cube([18,6,9],center=true);
                translate([0,0,2])
                  cube([16,4,7],center=true);
            }
        
        // motor connector pins
            
        translate([-6,0,0])
        for(i = [0:4])
        {
            translate([i*3,0,0])
                color("silver")
                    cylinder(h=4,r=0.5);
        }
        
        // control inputs
        
        translate([-4,-16,-3])
        for(i = [0:3])
        {
            translate([i*3,0,0])
            {
                color("black")
                    cube([3,3,3],center=true);
                color("silver")
                    cylinder(h=5,r=0.5);
            }
        }
        
        // power inputs
        
        translate([18,-12.5,-3])
        for(i = [0:3])
        {
            translate([0,i*3,0])
            {
                color("black")
                    cube([3,3,3],center=true);
                color("silver")
                    cylinder(h=5,r=0.5);
            }
        }
    }

    // ic
    
	color("black")
		translate([-3,-4,5])
			cube([24,10,9],center=true);

    // board
    
	color("green")
		difference()
		{
			cube([31,35,1],center=true);
            
            // board holes
            
			for(i = [-1:2:1])
				for(j = [-1:2:1])
				{
					translate([i*(31/2 - 2),j*(35/2 - 2),0])
						cylinder(h=2,d=3,center=true);
				}
		}
     
    // leds
        
    translate([-10,12,0])
        for (i = [0:3])
        {
            translate([i*5.5,0,0])
                led();
        }    
}

stepper(30);

!rotate([0,0,-90])stepper_driver();

*!led();


