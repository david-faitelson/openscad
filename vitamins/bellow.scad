module basic_triangle() 
{
	linear_extrude(height=1)
		polygon(points=[[-43.3,0],[43.3,0],[0,25]]);
}

module bellow(opening_angle=15, n=6)
{
	for(i=[0:1:5])
	{
		rotate([0,0,i*60])
			translate([0,25,0])
				basic_triangle();
	}
	
	for(j=[0:n-1])
	{
		rotate([0,0,60*j])
		{
			translate([0,0,-sin(opening_angle)*25*j])
			{
				for(i=[0:1:2])
				{
					rotate([0,0,i*120])
						translate([0,25,0]) 
						{
							rotate([-opening_angle,0,0])
								basic_triangle();
						}
				}
				
				translate([0,0,-sin(opening_angle)*25])
				rotate([0,0,60])
				for(i=[0:1:2])
				{
					rotate([0,0,i*120])
						translate([0,25,0])
							rotate([opening_angle,0,0])
								basic_triangle();
				}
			}
		}
	}
	
	translate([0,0,-sin(opening_angle)*25*n])
	for(i=[0:1:5])
	{
		rotate([0,0,i*60])
			translate([0,25,0])
				basic_triangle();
	}
}

function map(x,b1,e1,b2,e2) =
	b2 + (x-b1)*(e2-b2)/(e1-b1);


bellow_height = 70;

bellow_angle = asin(bellow_height/(7*25));

bellow(opening_angle=bellow_angle,n=7);

translate([50, 0, -bellow_height]) cube([5,5,bellow_height]);

