use <jst.scad>
use <vitamins.scad>

module solenoid_driver()
{
    color("green",0.99)
    rotate([0,0,-90])translate([-125.1,27.3,-0.5]) import("../electronics/solenoid-driver/solenoid-driver.stl",convexity=3); 
}


solenoid_driver();
