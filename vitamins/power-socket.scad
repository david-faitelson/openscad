
power_socket();

module power_socket()
{
    color("gray")
        difference()
        {
            union()
            {
                cylinder(h=4,d=10);
                intersection()
                {
                    cylinder(h=12.1,d=8);
                    translate([-4,-6.5/2,0])cube([8,6.5,12.1]);
                }
            }
            translate([0,0,-0.1])cylinder(h=4, d=6.5);
        }
    
    color("silver")
        cylinder(h=4, d=2);
    
    color("silver")
        translate([0,0,12.1])
        {
            translate([-1.1,2,0])cube([2.2,0.5,5.9]);
            translate([-1.1,-2.5,0])cube([2.2,0.5,4.0]);
        }
}

